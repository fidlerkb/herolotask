import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatMenuModule, MatButtonModule,MatDialog,MatDialogModule, MatInputModule,MatTable,MAT_DIALOG_DATA } from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatIconModule,MatIcon} from '@angular/material/icon';
import { FormsModule }   from '@angular/forms'; 
import { AppComponent } from './app.component';
import { HttpModule, Http }   from '@angular/Http';
import {MatExpansionModule} from '@angular/material/expansion';
import { TaskServiceService } from './taskservice.service';
import { HomeComponent } from './home/home.component'; 
import { BooksModule } from './books/books.module';
import { EditModalComponent } from './home/edit-modal/edit-modal.component';
import { UiPipesPipe } from './ui-pipes.pipe';
import { MaxStringPipe } from './max-string.pipe';
import { ServerService } from './server.service';
import { NavigationComponent } from './navigation/navigation.component';
import { RouterModule,Routes } from '@angular/router';
import { MainComponent } from './main/main.component';
import { FooterComponent } from './footer/footer.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    EditModalComponent,
    UiPipesPipe,
    MaxStringPipe,
    NavigationComponent,
    MainComponent,
    FooterComponent
    
  ],

  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    MatMenuModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatDialogModule,
    MatExpansionModule,
    HttpModule,

    RouterModule.forRoot([
      {path:'', component: MainComponent},
      {path:'Lib', component: HomeComponent},

    ])
  ],

  

  providers: [
    TaskServiceService,
    ServerService
  ],
  
  entryComponents:[EditModalComponent],
  bootstrap: [AppComponent]

})
export class AppModule { }
