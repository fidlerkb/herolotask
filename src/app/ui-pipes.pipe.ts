import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'capitilize'
})
export class UiPipesPipe implements PipeTransform {

  transform(value: any) {
    if(value){
      value = value.replace(/[^A-Za-z0-9]/gi, ' ');
      return value.replace(/\b\w/g, first => first.toLocaleUpperCase());
    }
    return value;
  }

}
