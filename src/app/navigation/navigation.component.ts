import { Component, OnInit } from '@angular/core';
import { TaskServiceService } from '../taskservice.service';
import { RouterModule, Router } from '@angular/router';

@Component({
  selector: 'navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  constructor(private service : TaskServiceService,private router: Router) { }

  ngOnInit() {
  }


  select(path: string) {
    this.router.navigate([path])
  }

}
