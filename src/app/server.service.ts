import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/Http';
import 'rxjs/Rx';
import { BooksModule } from './books/books.module';

@Injectable()
export class ServerService {

  constructor(private http : Http) { 
    
  }

  setDataToServer(data :any[]){
    return this.http.put('https://harolodb.firebaseio.com/data.json',data);
  }

  getDataFromServer(){
    return this.http.get('https://harolodb.firebaseio.com/data.json')
    .map(
      (responce : Response) => {
        const data = responce.json();
        return data;
      }
    );
  }

}
