import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Http, Headers, Response, HttpModule } from '@angular/http';
//import { error } from 'protractor';
import { BooksModule } from './books/books.module';
import { ServerService } from './server.service';


@Injectable()
export class TaskServiceService {

  constructor(private server: ServerService) {
    this.booksList = this.getData();
  }


  booksList: BooksModule[] = [];
  selectedBook: BooksModule;

  setData() {
    this.server.setDataToServer(this.booksList).subscribe
      (
      (Response) => console.log(Response),
      (error) => console.log(error)
      );
  }

  getData() {
    this.server.getDataFromServer().subscribe
      (
      (List: any[]) => this.booksList = List
      );
    return this.booksList;
  }

  setBookList(data) {
    localStorage.setItem('BookList', JSON.stringify(data))
  }

  getBookList() {
    return JSON.parse(localStorage.getItem('BookList'));
  }


  addNewBook(item: BooksModule) {
    this.booksList.push(item);
    this.setData()
  }

  deleteBook(book: BooksModule) {
    this.booksList = this.booksList.filter((item: BooksModule) => item.Title != book.Title)
    this.setData();
    this.getData();
    this.deleteSelectedBook();

  }

  editBook(_author, _title, _date) {
    let TempBook: BooksModule[] = [];
    TempBook = this._filterItems(this.selectedBook.Title)
    if (TempBook.length > 0) {
      TempBook[0].Title = _title;
      TempBook[0].Author = _author;
      TempBook[0].Date = _date;
    }
    this.setData();
    this.deleteSelectedBook();

  }

  getIndexOfEelement(book: BooksModule) {
    let index = this.booksList.indexOf(book);
    return index;
  }

  private _filterItems(title) {
    return this.booksList.filter((item) => {
      return item.Title == title;
    });
  }

  setSelectedBook(book: BooksModule) {
    this.selectedBook = book;
  }

  returnSelectefBook() {
    return this.selectedBook;
  }

  deleteSelectedBook() {
    this.selectedBook = null;
  }

  validateNewBook(title: string) {
    let validBook: BooksModule[] = [];
    validBook = this._filterItems(title);
    if (validBook.length > 0) {
      if(this.selectedBook ==null)
        return false;
      else if (title == this.selectedBook.Title) {
        return true;
      }
      return false;
    }
    return true;
  }

  validateDate(date: Date) {
    let _date = date.toString();
    let rgexp = /^(\d{1,2})-(\d{1,2})-(\d{4})$/;
    let isValidDate = rgexp.test(_date);
    return isValidDate;
  }



}
