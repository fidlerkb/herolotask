import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TaskServiceService } from '../../taskservice.service';
import { BooksModule } from '../../books/books.module';

@Component({
  selector: 'edit-modal',
  templateUrl: './edit-modal.component.html',
  styleUrls: ['./edit-modal.component.css']
})



export class EditModalComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<EditModalComponent>,private servise : TaskServiceService) {
    this.initEditedBook();
   }

  action: boolean = true;
  Author:string;
  Title:string;
  Date:Date;
  validTitle : boolean =true;
  isvalid:boolean
  dateValid : boolean = true;

  ngOnInit() {
    
  }

  Close(){
    this.dialogRef.close();
  }

  Add(){
    let Book = new BooksModule;
    Book.Author = this.Author; 
    Book.Title = this.Title; 
    Book.Date = this.Date; 
    this.validTitle = this.servise.validateNewBook(Book.Title)
    this.dateValid = this.servise.validateDate(Book.Date);
    this.isvalid = this.validinput();
    if(this.validTitle && this.isvalid && this.dateValid){
      this.servise.addNewBook(Book);   
      this.Close();
    }
  }

  edit(){
    this.validTitle = this.servise.validateNewBook(this.Title)
    this.isvalid = this.validinput();
    this.dateValid = this.servise.validateDate(this.Date);
    if(this.validTitle && this.isvalid && this.dateValid){
       this.servise.editBook(this.Author,this.Title,this.Date); 
       this.Close();  
    }
  }

  initEditedBook(){
    let book : BooksModule = this.servise.returnSelectefBook();
    if(book!==null)
    {
      this.action = false;
      this.Author = book.Author;
      this.Title = book.Title;
      this.Date = book.Date;
    }
  }

  validinput(){
    if(this.Author.length > 0 && this.Title.length > 0  && this.Date != null)
      return true;
  }


}
