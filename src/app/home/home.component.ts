import { Component, OnInit } from '@angular/core';
import { TaskServiceService } from '../taskservice.service';
import { BooksModule } from '../books/books.module';
import { NgModule } from '@angular/core';
import { MatIconRegistry, MatIconModule, MatDialog, MatDialogRef } from '@angular/material';
import { MAT_DIALOG_DATA } from '@angular/material';
import { EditModalComponent } from './edit-modal/edit-modal.component';
import { UiPipesPipe } from '../ui-pipes.pipe';
import { MaxStringPipe } from '../max-string.pipe';


@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(public dialog: MatDialog, private service: TaskServiceService) {
    this.list = this.service.getData();
    this.sortList();

  }

  list = this.service.booksList;
  selectedBook: BooksModule;
  

  ngOnInit() {
    
  }


  selectBook(item: BooksModule) {
    this.selectedBook = item;
    this.service.setSelectedBook(this.selectedBook);
  }

  EditBook() {
    this.service.setSelectedBook(this.selectedBook);
    let dialogRef = this.dialog.open(EditModalComponent, {
      width: '250px',
    })
    dialogRef.afterClosed().subscribe(result => {
      this.list = this.service.getData();
      this.sortList();
    });
  }

  DeleteBook() {
    this.service.deleteBook(this.selectedBook);
    this.service.setData();
    this.list = this.service.getData();
    this.sortList();
  } 

  AddBook() {
    this.service.deleteSelectedBook();
    let dialogRef = this.dialog.open(EditModalComponent, {
      width: '250px',
    })
    dialogRef.afterClosed().subscribe(result => {
      this.list = this.service.getData();
      this.sortList();
    });
  }

  sortList(){
    this.list.sort((a, b) => {
      if (a.Author < b.Author) return -1;
      else if (a.Author > b.Author) return 1;
      else return 0;
    })
  }

}
