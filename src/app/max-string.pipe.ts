import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'maxString'
})
export class MaxStringPipe implements PipeTransform {

  transform(value: any) {
    if(value.length > 25){
      return value.substr(0,25) + ' ...';
    }
    return value;
  }

} 
